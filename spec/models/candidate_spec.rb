# encoding: utf-8

require 'spec_helper'

describe Candidate do
  it "should have proper name" do
    expect(Candidate.new(name: "Ruby on Rails")).to have(1).error_on(:name)
    expect(Candidate.new(name: "Иванов Пётр Петрович")).to have(0).error_on(:name)
  end

  it "should have phone or e-mail in contact info" do
    expect(Candidate.new(contacts: "Ruby on Rails")).to have(1).error_on(:contacts)
    expect(Candidate.new(contacts: "+74951111111")).to have(0).error_on(:contacts)
    expect(Candidate.new(contacts: "test@ya.ru")).to have(0).error_on(:contacts)
  end

  it "should have skill set" do
    expect(Candidate.new).to have(1).error_on(:skills)
    expect(Candidate.new(skills: [Skill.new(name: "Ruby on Rails")])).to have(0).error_on(:skills)
  end

  it "should save skills correctly" do
    Skill.create!(name: "Ruby on Rails")
    Candidate.should have(:no).records
    Candidate.create!(name: "Иванов Пётр Петрович",
                      salary: 20000,
                      contacts: "test@ya.ru",
                      searching: true,
                      skills: Skill.find_all_by_name("Ruby on Rails"))
    Candidate.should have(1).record
    expect(Candidate.first.skills[0].name).to eq("Ruby on Rails")
  end

  it "should match candidates" do
    Skill.create!(name: "Ruby on Rails")
    Skill.create!(name: "Java")
    Vacancy.should have(:no).records
    vacancy1 = Vacancy.create!(name: "Ruby on Rails Developer",
                               salary: 20000,
                               contacts: "Find us on Tweeter",
                               valid_till: Date.today + 1,
                               skills: Skill.find_all_by_name(["Ruby on Rails", "Java"]))

    vacancy2 = Vacancy.create!(name: "Ruby on Rails Developer",
                               salary: 20000,
                               contacts: "Find us on Tweeter",
                               valid_till: Date.today + 1,
                               skills: Skill.find_all_by_name("Java"))

    vacancy3 = Vacancy.create!(name: "Ruby on Rails Developer",
                               salary: 20000,
                               contacts: "Find us on Tweeter",
                               valid_till: Date.today - 10,
                               skills: Skill.find_all_by_name("Ruby on Rails"))


    candidate = Candidate.create!(name: "Иванов Пётр Петрович",
                                  salary: 20000,
                                  contacts: "test@ya.ru",
                                  searching: true,
                                  skills: Skill.find_all_by_name("Ruby on Rails"))
    candidate.vacancies.should match_array([vacancy1])
  end
end
