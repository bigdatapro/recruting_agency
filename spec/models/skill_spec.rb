require 'spec_helper'

describe Skill do
  it "should have unique name" do
    Skill.create!(name: "Java")

    expect(Skill.new(name: "Java")).to have(1).error_on(:name)
  end
end
