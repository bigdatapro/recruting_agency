# encoding: utf-8

require 'spec_helper'

describe Vacancy do
  it "should have a name" do
    expect(Vacancy.new).to have(1).error_on(:name)
    expect(Vacancy.new(name: "Ruby on Rails Developer")).to have(0).error_on(:name)
  end

  it "should have skill set" do
    expect(Vacancy.new).to have(1).error_on(:skills)
    expect(Vacancy.new(skills: [Skill.new(name: "Ruby on Rails")])).to have(0).error_on(:skills)
  end

  it "should save skills correctly" do
    Skill.create!(name: "Ruby on Rails")
    Vacancy.should have(:no).records
    Vacancy.create!(name: "Ruby on Rails Developer",
                    salary: 20000,
                    contacts: "Find us on Tweeter",
                    valid_till: Date.today,
                    skills: Skill.find_all_by_name("Ruby on Rails"))
    Vacancy.should have(1).record
    expect(Vacancy.first.skills[0].name).to eq("Ruby on Rails")
  end

  it "should match candidates" do
    Skill.create!(name: "Ruby on Rails")
    Skill.create!(name: "Java")
    Candidate.should have(:no).records
    candidate1 = Candidate.create!(name: "Иванов Пётр Петрович",
                                   salary: 20000,
                                   contacts: "test@ya.ru",
                                   searching: true,
                                   skills: Skill.find_all_by_name("Ruby on Rails"))

    candidate2 = Candidate.create!(name: "Иванов Пётр Петрович",
                                   salary: 20000,
                                   contacts: "test@ya.ru",
                                   searching: true,
                                   skills: Skill.find_all_by_name("Java"))

    candidate3 = Candidate.create!(name: "Иванов Пётр Петрович",
                                   salary: 20000,
                                   contacts: "test@ya.ru",
                                   searching: false,
                                   skills: Skill.find_all_by_name("Ruby on Rails"))

    vacancy = Vacancy.create!(name: "Ruby on Rails Developer",
                              salary: 20000,
                              contacts: "Find us on Tweeter",
                              valid_till: Date.today,
                              skills: Skill.find_all_by_name("Ruby on Rails"))
    vacancy.candidates.should match_array([candidate1])
  end
end
