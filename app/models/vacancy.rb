class Vacancy < ActiveRecord::Base
  attr_accessible :contacts, :name, :salary, :valid_till, :skills, :skill_ids
  has_and_belongs_to_many :skills, :join_table => 'vacancies_skills'

  validates_presence_of :name
  validates_presence_of :salary
  validates_presence_of :contacts
  validates_presence_of :valid_till
  validates_presence_of :skills

  def candidates
    Candidate.searching.with_skills(self.skills).order("salary")
  end

  scope :valid, lambda { where("valid_till > ?", Time.zone.now ) }
  scope :with_skills, lambda { |skills|
    {
        :include => :skills,
        :conditions => [ "vacancies_skills.skill_id IN (?)", skills.map(&:id) ]
    }
  }
end
