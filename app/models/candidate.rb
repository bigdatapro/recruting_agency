# encoding: utf-8

class Candidate < ActiveRecord::Base
  attr_accessible :contacts, :name, :salary, :searching, :skills, :skill_ids
  has_and_belongs_to_many :skills

  validates_presence_of :name
  validates_presence_of :contacts
  validates_presence_of :salary
  validates_inclusion_of :searching, :in => [true, false]
  validates_presence_of :skills

  validates :name, :format => { :with => /\A\p{Cyrillic}+ \p{Cyrillic}+ \p{Cyrillic}+\z/,
                                :message => "Укажите Ваши ФИО на русском языке" }

  validates :contacts, :format => { :with => /(\b[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b)|(\b[78]\d{10}\b)/,
                                :message => "Укажите Ваш e-mail и/или номер телефона" }

  scope :searching, where(:searching => true)
  scope :with_skills, lambda { |skills|
    {
        :include => :skills,
        :conditions => [ "candidates_skills.skill_id IN (?)", skills.map(&:id) ]
    }
  }

  def vacancies
    Vacancy.valid.with_skills(self.skills).order("salary DESC")
  end
end
