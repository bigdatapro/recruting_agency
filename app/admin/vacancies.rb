# encoding: utf-8

ActiveAdmin.register Vacancy do
  index do
    column :name
    column :contacts
    column :salary
    column :created_at
    column :valid_till

    column :skills do |candidate|
      candidate.skills.map { |s| s.name }.join('<br />').html_safe
    end

    column do |vacancy|
      link_to('Подходящие работники', candidates_vacancy_path(vacancy))
    end
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :contacts
      f.input :salary
      f.input :valid_till, :as => :date_picker
    end

    f.inputs :skills do # Make a panel that holds inputs for skills
      f.input :skills, as: :check_boxes, collection: Skill.all # Use formtastic to output my collection of checkboxes
    end
    f.actions # Include the default actions
  end

  member_action :candidates do
    @vacancy = Vacancy.find(params[:id])
    @page_title = "Кандидаты на вакансию #{@vacancy.name}"
  end
end
