# encoding: utf-8

ActiveAdmin.register Candidate do
  index do
    column :name
    column :contacts
    column :searching
    column :salary

    column :skills do |candidate|
      candidate.skills.map { |s| s.name }.join('<br />').html_safe
    end

    column do |candidate|
      link_to('Подходящие вакансии', vacancies_candidate_path(candidate))
    end
    default_actions
  end

  form do |f|
    f.inputs # Include the default inputs
    f.inputs :skills do # Make a panel that holds inputs for skills
      f.input :skills, as: :check_boxes, collection: Skill.all # Use formtastic to output my collection of checkboxes
    end
    f.actions # Include the default actions
  end

  member_action :vacancies do
    @candidate = Candidate.find(params[:id])
    @page_title = "Вакансии для работника #{@candidate.name}"
  end

end
