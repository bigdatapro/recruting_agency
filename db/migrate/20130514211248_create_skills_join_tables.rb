class CreateSkillsJoinTables < ActiveRecord::Migration
  def up
    create_table :vacancies_skills, :id => false do |t|
      t.integer :vacancy_id
      t.integer :skill_id
    end

    add_index :vacancies_skills, [:vacancy_id, :skill_id]

    create_table :candidates_skills, :id => false do |t|
      t.integer :candidate_id
      t.integer :skill_id
    end

    add_index :candidates_skills, [:candidate_id, :skill_id]
  end

  def down
    drop_table :vacancies_skills
    drop_table :candidates_skills
  end
end
