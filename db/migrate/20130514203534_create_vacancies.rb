class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.string :name
      t.datetime :valid_till
      t.integer :salary
      t.string :contacts

      t.timestamps
    end
  end
end
